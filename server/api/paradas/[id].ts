import diccionario_paradas from '../../data/diccionario-paradas.json'

export default defineEventHandler((event) => {
  const id = getRouterParam(event, 'id')
  const id_string = id || '1'

  return diccionario_paradas[id_string]
})
