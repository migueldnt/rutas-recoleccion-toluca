import delegaciones from '../data/delegaciones.json'

export default defineEventHandler(() => delegaciones)
