import diccionario_rutas from '../../data/diccionario-rutas.json'

export default defineEventHandler((event) => {
  const id = getRouterParam(event, 'id')
  const id_string = id || '1'

  return diccionario_rutas[id_string]
})
