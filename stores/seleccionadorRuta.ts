import { defineStore } from 'pinia'

export const useSeleccionadorRutaStore = defineStore('seleccionadorRuta', {
  state: () => ({
    rutaSeleccionada: '',
    info: {
      NODEL: undefined,
      NOMDEL: undefined,
      CVEUT: undefined,
      NOMUT: undefined,
      LUNES: undefined,
      MARTES: undefined,
      MIERCOLES: undefined,
      JUEVES: undefined,
      VIERNES: undefined,
      SABADO: undefined,
      DOMINGO: undefined,
      CALLES: undefined,
      ENTRADA: undefined,
      SALIDA: undefined,
      ECO: undefined,
      KM: undefined,
      NODOS: undefined,
      NO_DEL: undefined,
      CVRUTA: undefined,
    },
  }),
})
