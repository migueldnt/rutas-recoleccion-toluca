import { defineStore } from 'pinia'

export const useLayerSwitcherStore = defineStore('layerSwitcher', {
  state: () => ({
    delegacionesVisible: true,
    utbsVisible: false,
    contenedoresVisible: false,
    difVisible: false,
    puntosFijosVisible: false,
    conveniosVisible: false,
    rellenoVisible: false,
    tiraderosVisible: false,
  }),
  actions: {
    alternarDelegacionesUtbsVisible(
      layerTurnOn: 'delegaciones' | 'utbs' = 'delegaciones'
    ) {
      if (layerTurnOn === 'delegaciones') {
        this.delegacionesVisible = true
        this.utbsVisible = false
      }
      else {
        this.delegacionesVisible = false
        this.utbsVisible = true
      }
    },
  },
})
