# Etapa de construcción
FROM node:18.18-alpine as build

WORKDIR /app

COPY package*.json ./
RUN npm ci
COPY . .
RUN npm run build

# Etapa de ejecución
FROM node:18.18-alpine

WORKDIR /app

COPY --from=build /app/package*.json ./
RUN npm ci
COPY --from=build /app/.output ./output

CMD node ./output/server/index.mjs
