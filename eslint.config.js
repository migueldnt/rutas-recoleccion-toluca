import antfu from '@antfu/eslint-config'

export default antfu(
  {
    vue: true,
    stylistic: {
      indent: 2, // 4, or 'tab'
      quotes: 'single', // or 'double',
    },
    ignores: ['node_modules', '.nuxt', '.output', 'server/data'],
    // formatters: true,
  },
  {
    rules: {
      'style/comma-dangle': 'off',
      'vue/html-self-closing': [
        'error',
        {
          html: {
            void: 'always',
            normal: 'never',
          },
        },
      ],
    },
  }
)
