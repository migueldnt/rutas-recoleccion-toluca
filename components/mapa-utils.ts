import maplibregl from 'maplibre-gl'

function agregarPopupALayer(
  map: maplibregl.Map,
  layerName: string,
  contentCallback: (feature: any) => string
) {
  map.on('click', layerName, (e: any): void => {
    const coordinates = e.features[0].geometry.coordinates.slice()
    const description = contentCallback(e.features[0])
    new maplibregl.Popup()
      .setLngLat(coordinates)
      .setHTML(`<b>${description}</b>`)
      .addTo(map)
  })
  // Change the cursor to a pointer when the mouse is over the places layer.
  map.on('mouseenter', layerName, () => {
    map.getCanvas().style.cursor = 'pointer'
  })

  // Change it back to a pointer when it leaves.
  map.on('mouseleave', layerName, () => {
    map.getCanvas().style.cursor = ''
  })
}

export { agregarPopupALayer }
